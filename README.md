# Dékroketteur

Ce projet est un démineur basé en lua.

## Comment lancer le projet

`love --console . 15 25 40`

- le premier nombre (15) correspond au nombre de colonnes
- le second au nombre de lignes 
- le dernier au nombre de mines
