local Demineur = require("demineur")
local socket = require "socket"
local node_red = require "node_red"
local udp = socket.udp()
local json = require "json"
local menu = true
time = 0


node_red.onReceive = function(data)
    local t = json.decode(data)
    for index, value in ipairs(t) do
        print(value)
    -- more of these funky match paterns!
        local p_f, x, y, derien = value:match("^(....) (%d*) (%d*) ?(%d*)")
        if p_f == "code" then
            demineur = Demineur:new(tonumber(x), tonumber(y), tonumber(derien))
        elseif p_f == "flag" then
            demineur:drapeau(tonumber(x), tonumber(y))
        else
            demineur:play(tonumber(x), tonumber(y))
        end
    end
end

function hslToRgb(h, s, l, a)
	local r, g, b
	if s == 0 then
		r, g, b = l, l, l -- achromatic
	else
		function hue2rgb(p, q, t)
			if t < 0   then t = t + 1 end
			if t > 1   then t = t - 1 end
			if t < 1/6 then return p + (q - p) * 6 * t end
			if t < 1/2 then return q end
			if t < 2/3 then return p + (q - p) * (2/3 - t) * 6 end
			return p
		end
		local q
		if l < 0.5 then q = l * (1 + s) else q = l + s - l * s end
		local p = 2 * l - q

		r = hue2rgb(p, q, h + 1/3)
		g = hue2rgb(p, q, h)
		b = hue2rgb(p, q, h - 1/3)
	end

	return r, g, b, a
end

function love.load(arg)
-- arg[1] correspond au nombre de colonnes
-- arg[2] correspond au nombre de lignes
-- arg[3] correspond au nombre de krokettes
    demineur = Demineur:new(tonumber(arg[1] or 10), tonumber(arg[2] or 10), tonumber(arg[3] or 20))
    udp:settimeout(0)
    udp:setsockname('0.0.0.0', 12345)
    love.graphics.setBackgroundColor(0,0,0,0)
end

function love.draw()
    demineur:draw()
end

function love.mousepressed(x, y, button, istouch)
    if button == 1 then
        Index_casex = math.floor((x-28)/60)
        Index_casey = math.floor((y-95)/30)
        print(Index_casex, Index_casey)
        demineur:play(Index_casex, Index_casey)
        -- table.insert(Case, ""..Index_casex.."-"..Index_casey)
        -- print(Case[1], Case[2])
    end
end

function love.update(dt)
    node_red:update(dt)
    time = time + dt
    data, msg_or_ip, port_or_nil = udp:receivefrom()
	if data then
        print(data)
		-- more of these funky match paterns!
		local p_f, x, y, derien = data:match("^(....) (%d*) (%d*) ?(%d*)")
        if p_f == "code" then
            demineur = Demineur:new(tonumber(x), tonumber(y), tonumber(derien))
        elseif p_f == "flag" then
            demineur:drapeau(tonumber(x), tonumber(y))
        else
            demineur:play(tonumber(x), tonumber(y))
        end
    end
end

function terminatour()
    print("perdu1")
    node_red:send("ws/end_game/", "perdu")
    print("perdu2")
end